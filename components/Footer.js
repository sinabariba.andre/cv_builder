import React from "react";
import Head from "next/head";
const years = new Date().getFullYear()

const Footer = () => (
  <div>
    <Head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link
        rel="stylesheet"
        href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"
      />
      <link
        rel="stylesheet"
        href="/assets/css/Footer-with-button-logo.css"
      ></link>
    </Head>
    <footer id="myFooter">
      <div className="container-fluid" style={{marginLeft:0}}>
        <div className="row">
          <div className="col-sm-3">
            <h2 className="logo">
              <a href="#"> LOGO </a>
            </h2>
          </div>
          <div className="col-sm-2">
            <h5>Get started</h5>
            <ul>
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">Sign up</a>
              </li>
              <li>
                <a href="#">Downloads</a>
              </li>
            </ul>
          </div>
          <div className="col-sm-2">
            <h5>About us</h5>
            <ul>
              <li>
                <a href="#">Company Information</a>
              </li>
              <li>
                <a href="#">Contact us</a>
              </li>
              <li>
                <a href="#">Reviews</a>
              </li>
            </ul>
          </div>
          <div className="col-sm-2">
            <h5>Support</h5>
            <ul>
              <li>
                <a href="#">FAQ</a>
              </li>
              <li>
                <a href="#">Help desk</a>
              </li>
              <li>
                <a href="#">Forums</a>
              </li>
            </ul>
          </div>
          <div className="col-sm-3">
            <div className="social-networks">
              <a href="#" className="twitter">
                <i className="fa fa-twitter"></i>
              </a>
              <a href="#" className="facebook">
                <i className="fa fa-facebook"></i>
              </a>
              <a href="#" className="google">
                <i className="fa fa-google-plus"></i>
              </a>
            </div>
            <button type="button" className="btn btn-default">
              Contact us
            </button>
          </div>
        </div>
      </div>
      <div className="footer-copyright">
        <p>© {years} Copyright Text </p>
      </div>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </div>
);

export default Footer;
