import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useRouter } from "next/router";

import { connect, useDispatch, useSelector } from "react-redux";
import {
  Button,
  Label,
  Input,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import {
  months,
  generateArrayOfYears
} from "../global/global.function";
import swal from "@sweetalert/with-react";
import axios from "axios";


const FormEducation = () => {
  const { univ, province, titles, faculties } = useSelector(
    (state) => state.API
  );

  const {
    register,
    formState: { errors },
    reset,
    handleSubmit,
  } = useForm();

  const dispatch = useDispatch();
  const handleChange = async (id) => {
    const res = await axios
      .get(
        `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${id}`
      )
      .then((res) => {
        dispatch({
          type: "GET_CITY",
          payload: res.data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(async () => {
  });

  const onSubmit = (data, e) => {
    e.preventDefault();
    dispatch({
      type: "ADD_EDUCATION",
      payload: [data],
    });
    swal("Good job!", "Education Added!", "success");
  };
  const years = generateArrayOfYears();
  const router = useRouter();

 

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col">
          <h2 className="text-center">EDUCATION</h2>
          <hr />
          <form id="eduForm" onSubmit={handleSubmit(onSubmit)}>
            <div className="row my-3">
              <div className="col">
                <Label for="school">School</Label>
                <Input
                  type="select"
                  {...register("school", { required: true })}
                >
                  <option key="" value="">
                    -Select School-
                  </option>
                  {Object.values(univ).map((item) => {
                    return (
                      <option key={item.domains} value={item.name}>
                        {item.name}
                      </option>
                    );
                  })}
                </Input>
                <span>
                  {errors.school?.type === "required" && "School is required"}
                </span>
              </div>
              <div className="col">
                <Label for="state">State province</Label>
                <Input
                  id="state"
                  type="select"
                  onChange=""
                  {...register("state", { required: true })}
                >
                  {Object.values(province.provinsi).map((item) => (
                    <option key={item.id} value={item.nama}>
                      {item.nama}
                    </option>
                  ))}
                </Input>
                <span>
                  {errors.state?.type === "required" && "State is required"}
                </span>
              </div>
            </div>
            <div className="row my-3">
              <div className="col">
                <Label for="city">City</Label>
                <Input {...register("city", { required: true })} />
                <span>{errors.city && "City is required"}</span>
              </div>
              <div className="col">
                <Label for="title">Program</Label>
                <Input type="select" {...register("title", { required: true })}>
                  <option value="">-Select Program-</option>
                  {Object.values(titles).map((item) => {
                    return (
                      <option key={item.id} value={item.title}>
                        {item.title}
                      </option>
                    );
                  })}
                </Input>
                <span>{errors.title && "Title is required"}</span>
              </div>
            </div>
           
            <div className="row my-3">
              <div className="col">
                <Label for="startmonth">Start Month</Label>
                <Input
                  type="select"
                  {...register("startmonth", { required: true })}
                >
                  <option value="">-Select month-</option>
                  {months.map((e) => {
                    return (
                      <option value={e} key={e}>
                        {e}
                      </option>
                    );
                  })}
                  <option value="">-Dont show month-</option>
                </Input>
                <span>
                  {errors.startmonth?.type === "required" &&
                    "Start Month is required"}
                </span>
              </div>
              <div className="col">
                <Label for="startyear">Start Year</Label>
                <Input
                  type="select"
                  {...register("startyear", { required: true })}
                >
                  <option value="">-Select year-</option>
                  <option value="">-Dont show year-</option>

                  {years.map((e) => {
                    return (
                      <option value={e} key={e}>
                        {e}
                      </option>
                    );
                  })}
                </Input>
                <span>{errors.startyear && "Start Year is required"}</span>
              </div>
            </div>
            <div className="row my-3">
              <div className="col">
                <Label for="endmonth">End Month</Label>
                <Input
                  type="select"
                  {...register("endmonth", { required: true })}
                >
                  <option value="">-Select month-</option>
                  {months.map((e) => {
                    return (
                      <option value={e} key={e}>
                        {e}
                      </option>
                    );
                  })}
                  <option value="">-Dont show month-</option>
                </Input>
                <span>
                  {errors.endmonth?.type === "required" &&
                    "End Month is required"}
                </span>
              </div>
              <div className="col">
                <Label for="endyear">End Year</Label>
                <Input
                  type="select"
                  {...register("endyear", { required: true })}
                >
                  <option value="">-Select year-</option>
                  <option value="">-Dont show year-</option>

                  {years.map((e) => {
                    return (
                      <option value={e} key={e}>
                        {e}
                      </option>
                    );
                  })}
                </Input>
                <span>{errors.endyear && "End Year is required"}</span>
              </div>
            </div>

            <button type="submit" style={{marginRight:'20px'}} className="btn btn-pastel btn-custom" >Save</button>
            <input type="reset" style={{marginRight:'20px'}} className="btn btn-danger btn-custom" />
            <button type="button" className="btn btn-success btn-custom" onClick={()=>router.push('/experience')}>Next</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default connect(null, null)(FormEducation);
