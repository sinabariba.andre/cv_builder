import React, { useState } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { months, generateArrayOfYears } from "../global/global.function";
import swal from "@sweetalert/with-react";
import Link from "next/link";

const mapStateToProps = (state) => ({
  experience: state.experience,
});
const preventDefault = (f) => (e) => {
  e.preventDefault();
  f(e);
};

const FormExperience = ({ dispatch }) => {
  const years = generateArrayOfYears();

  const router = useRouter();
  const [employer, setEmployer] = useState("");
  const [jobtitle, setJobtitle] = useState("");
  const [city, setCity] = useState("");
  const [phone, setPhone] = useState("");
  const [startmonth, setStartmonth] = useState("");
  const [startyear, setStartyear] = useState("");
  const [endmonth, setEndmonth] = useState("");
  const [endyear, setEndyear] = useState("");
  const [career, setCareer] = useState("");
  const [jobdesc, setJobdesc] = useState("");
  const handleParam = (setValue) => (e) => setValue(e.target.value);

  const handleSubmit = preventDefault(() => {
    dispatch({
      type: "ADD_EXPERIENCE",
      payload: [{
        employer,
        jobtitle,
        city,
        phone,
        startmonth,
        startyear,
        endmonth,
        endyear,
        career,
        jobdesc,
      }],
    });
    swal("Good job!", "Experience Added!", "success");
    
  });
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col">
          <h2 className="text-center">EXPERIENCE</h2>
          <hr />
          <Form onSubmit={handleSubmit}>
            <div className="row my-3">
              <div className="col">
                <Label for="">Employer</Label>
                <Input
                  type="text"
                  name="employer"
                  value={employer}
                  onChange={handleParam(setEmployer)}
                  id="employer"
                  placeholder="Ex : PT TOBA CEMERLANG"
                  required
                />
              </div>
              <div className="col ">
                <Label for="jobtitle">Job title</Label>
                <Input
                  type="text"
                  name="jobtitle"
                  value={jobtitle}
                  onChange={handleParam(setJobtitle)}
                  id="jobtitle"
                  placeholder="Ex : Manager"
                  required
                />
              </div>
            </div>
            <div className="row my-1">
              <div className="col">
                <Label for="city">City</Label>
                <Input
                  type="text"
                  name="city"
                  value={city}
                  onChange={handleParam(setCity)}
                  id="city"
                  placeholder="Ex : Samosir"
                  required
                />
              </div>
              <div className="col">
                <Label for="phone">Phone number</Label>
                <Input
                  type="text"
                  name="phone"
                  value={phone}
                  onChange={handleParam(setPhone)}
                  id="phone"
                  placeholder="e.g +6281258483xxx"
                  required
                />
              </div>
            </div>

            <div className="row my-1">
              <div className="col-md-3">
                <Label for="startdate">Tanggal Mulai</Label>
                <Input
                  type="select"
                  name="startmonth"
                  value={startmonth}
                  onChange={handleParam(setStartmonth)}
                  id="startmonth"
                >
                  {months.map((e) => {
                    return <option key={e}>{e}</option>;
                  })}
                  <option value="">-Jangan tampilkan bulan-</option>
                </Input>
              </div>
              <div className="col-md-3 ">
                <Label for="startdate">Year</Label>
                <Input
                  type="select"
                  name="startyear"
                  value={startyear}
                  onChange={handleParam(setStartyear)}
                  id="startmonth"
                >
                  <option value="">-Select-</option>
                  {years.map((e) => {
                    return <option key={e}>{e}</option>;
                  })}
                </Input>
              </div>
              <div className="col-md-3">
                <Label for="startdate">Tanggal Akhir</Label>
                <Input
                  type="select"
                  name="endmonth"
                  value={endmonth}
                  onChange={handleParam(setEndmonth)}
                  id="endmonth"
                >
                  {months.map((e) => {
                    return <option key={e}>{e}</option>;
                  })}
                  <option value="">-Jangan tampilkan bulan-</option>
                </Input>
              </div>
              <div className="col-md-3 ">
                <Label for="startdate">Year</Label>
                <Input
                  type="select"
                  name="endyear"
                  value={endyear}
                  onChange={handleParam(setEndyear)}
                  id="endyear"
                >
                  <option value="">-Select-</option>
                  {years.map((e) => {
                    return <option key={e}>{e}</option>;
                  })}
                </Input>
              </div>
            </div>
            <FormGroup>
              <Label for="career">Career Field</Label>
              <Input
                type="select"
                name="career"
                value={career}
                onChange={handleParam(setCareer)}
                id="career"
                required
              >
                <option>Accountant</option>
                <option>Staff Officer</option>
                <option>Manager</option>
                <option>Human Resources Department</option>
                <option>dll</option>
              </Input>
            </FormGroup>
            <div className="col ">
              <Label for="jobdesc">Job description</Label>
              <Input
                type="textarea"
                name="jobdesc"
                value={jobdesc}
                onChange={handleParam(setJobdesc)}
                id="jobdesc"
                placeholder="Describe your job here"
                rows="2"
              />
            </div>
            <br />
            <Button type="submit" style={{marginRight:'20px'}} className="btn-pastel">
              Save 
            </Button>
            <input type="reset" style={{marginRight:'20px'}} className="btn btn-danger btn-custom" />
            <Button type="button" className="btn btn-success btn-custom" onClick={()=>router.push('/skill')}>Next</Button>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps, null)(FormExperience);
