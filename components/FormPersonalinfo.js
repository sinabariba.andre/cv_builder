import React, { useState } from "react";
import { useRouter } from "next/router";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { connect } from "react-redux";
import swal from "@sweetalert/with-react";

const mapStateToProps = (state) => ({
  person: state.person,
});

const preventDefault = (f) => (e) => {
  e.preventDefault();
  f(e);
};

const FormPersonalinfo = ({ dispatch }) => {
  const router = useRouter();
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [address, setAddress] = useState("");
  const [city, setCity] = useState("");
  const [zipcode, setZipcode] = useState("");
  const [country, setCountry] = useState("");
  const [email, setEmail] = useState("");
  const [phonenumber, setPhonenumber] = useState("");
  const handleParam = (setValue) => (e) => setValue(e.target.value);

  const handleSubmit = preventDefault(() => {
    dispatch({
      type: "ADD_PERSON",
      payload: {
        fname,
        lname,
        address,
        city,
        zipcode,
        country,
        email,
        phonenumber,
      },
    });
    swal("Good job!", "", "success");
    router.push("/education");
  });

  return (
    <div className="container personal-info resume">
      <div className="row justify-content-center">
        <div className="col">
          <h2 className="text-center">COMPLETE YOUR RESUME</h2>
          <hr />
          <Form onSubmit={handleSubmit}>
            <div className="row my-3">
              <div className="col">
                <Label for="fname">First name</Label>
                <Input
                  type="text"
                  name="fname"
                  value={fname}
                  onChange={handleParam(setFname)}
                  id="fname"
                  placeholder="Ex : Andre"
                  required
                />
              </div>
              <div className="col ">
                <Label for="lname">Last name</Label>
                <Input
                  type="text"
                  name="lname"
                  value={lname}
                  onChange={handleParam(setLname)}
                  id="lname"
                  placeholder="Ex : Sinabariba"
                  required
                />
              </div>
            </div>
            <FormGroup>
              <Label for="address">Adress</Label>
              <Input
                type="text"
                name="address"
                value={address}
                onChange={handleParam(setAddress)}
                id="address"
                placeholder="Ex : Jl. Gatsu 57"
                required
              />
            </FormGroup>
            <div className="row my-1">
              <div className="col ">
                <Label for="city">City</Label>
                <Input
                  type="text"
                  name="city"
                  value={city}
                  onChange={handleParam(setCity)}
                  id="city"
                  placeholder="Ex : Jakarta"
                  required
                />
              </div>
              <div className="col">
                <Label for="zipcode">ZIP Code</Label>
                <Input
                  type="text"
                  name="zipcode"
                  value={zipcode}
                  onChange={handleParam(setZipcode)}
                  id="zipcode"
                  required
                  placeholder="Ex : 551231"
                />
              </div>
              <div className="col">
                <Label for="country">Country</Label>
                <Input
                  type="text"
                  name="country"
                  value={country}
                  onChange={handleParam(setCountry)}
                  id="country"
                  placeholder="Ex : Dubai"
                  required
                />
              </div>
            </div>
            <div className="row my-1">
              <div className="col ">
                <Label for="email">Email</Label>
                <Input
                  type="email"
                  name="email"
                  value={email}
                  onChange={handleParam(setEmail)}
                  id="email"
                  placeholder="Ex : email@example.com"
                  required
                />
              </div>
              <div className="col">
                <Label for="phone">Phone number</Label>
                <Input
                  type="text"
                  name="phone"
                  value={phonenumber}
                  onChange={handleParam(setPhonenumber)}
                  id="phone"
                  placeholder="Ex : +6281258483xxx"
                  required
                />
              </div>
            </div>
            <br />
            <Button type="submit" className="btn-pastel">Save &amp; Next</Button>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps, null)(FormPersonalinfo);
