import React, { useState } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";

const mapStateToProps = (state) => ({
  skill: state.skill,
});
const preventDefault = (f) => (e) => {
  e.preventDefault();
  f(e);
};

const FormSkill = ({ dispatch }) => {
  const router = useRouter();
  const [skill1, setSkill1] = useState("");
  const [level1, setLevel1] = useState("");
  const [skill2, setSkill2] = useState("");
  const [level2, setLevel2] = useState("");
  const [skill3, setSkill3] = useState("");
  const [level3, setLevel3] = useState("");
  const handleParam = (setValue) => (e) => setValue(e.target.value);

  const handleSubmit = preventDefault(() => {
    dispatch({
      type: "ADD_SKILL",
      payload: {
        skill1,
        level1,
        skill2,
        level2,
        skill3,
        level3,
      },
    });
    router.push("/summary");
  });
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col">
          <h2 className="text-center">SKILL</h2>
          <hr/>
          <Form onSubmit={handleSubmit}>
            <div className="row my-3">
              <div className="col">
                <Label for="skill-1">Skill</Label>
                <Input
                  type="text"
                  name="skill1"
                  value={skill1}
                  onChange={handleParam(setSkill1)}
                  id="skill1"
                  placeholder="Ex : Microsoft Word"
                  autoFocus
                  required
                />
              </div>
              <div className="col ">
                <Label for="level1">Level</Label>
                <Input
                  type="select"
                  name="level1"
                  value={level1}
                  onChange={handleParam(setLevel1)}
                  id="level1"
                >
                  <option></option>
                  <option>Novice</option>
                  <option>Beginner</option>
                  <option>Skillful</option>
                  <option>Experienced</option>
                  <option>Expert</option>
                </Input>
              </div>
            </div>
            <div className="row my-3">
              <div className="col">
                <Label for="skill2">Skill</Label>
                <Input
                  type="text"
                  name="skill2"
                  value={skill2}
                  onChange={handleParam(setSkill2)}
                  id="skill2"
                  placeholder="Ex : Microsoft Word"
                />
              </div>
              <div className="col ">
                <Label for="level2">Level</Label>
                <Input
                  type="select"
                  name="level2"
                  value={level2}
                  onChange={handleParam(setLevel2)}
                  id="level2"
                >
                  <option></option>
                  <option>Novice</option>
                  <option>Beginner</option>
                  <option>Skillful</option>
                  <option>Experienced</option>
                  <option>Expert</option>
                </Input>
              </div>
            </div>
            <div className="row my-3">
              <div className="col">
                <Label for="skill3">Skill</Label>
                <Input
                  type="text"
                  name="skill3"
                  value={skill3}
                  onChange={handleParam(setSkill3)}
                  id="skill3"
                  placeholder="Ex : Microsoft Word"
                />
              </div>
              <div className="col ">
                <Label for="level3">Level</Label>
                <Input
                  type="select"
                  name="level3"
                  value={level3}
                  onChange={handleParam(setLevel3)}
                  id="level3"
                >
                  <option></option>
                  <option>Novice</option>
                  <option>Beginner</option>
                  <option>Skillful</option>
                  <option>Experienced</option>
                  <option>Expert</option>
                </Input>
              </div>
            </div>
            <br />
            <Button type="submit" className="btn-pastel">Save &amp; Next</Button>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps, null)(FormSkill);
