import React, { useState } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";

const mapStateToProps = (state) => ({
  summary: state.summary,
});
const preventDefault = (f) => (e) => {
  e.preventDefault();
  f(e);
};

const FormSummary = ({ dispatch }) => {
  const router = useRouter();
  const [summary, setSummary] = useState("");
  const [file, setFile] = useState("");
  const [image, setImage] = useState("");
  const [error, setError] = useState("");
  const handleParam = (setValue) => (e) => setValue(e.target.value);

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    const reader = new FileReader();
    if (file) {
      reader.onload = function (e) {
        var img = document.createElement("img");
        img.src = e.target.result;

        img.onload = function (event) {
          // Dynamically create a canvas element
          var canvas = document.createElement("canvas");
          var ctx = canvas.getContext("2d");
          ctx.drawImage(img, 0, 0);

          var MAX_WIDTH = 400;
          var MAX_HEIGHT = 300;
          var width = img.width;
          var height = img.height;

          // Add the resizing logic
          if (width > height) {
            if (width > MAX_WIDTH) {
              height *= MAX_WIDTH / width;
              width = MAX_WIDTH;
            }
          } else {
            if (height > MAX_HEIGHT) {
              width *= MAX_HEIGHT / height;
              height = MAX_HEIGHT;
            }
          }
          // Actual resizing
          canvas.width = width;
          canvas.height = height;
          var ctx = canvas.getContext("2d");
          ctx.drawImage(img,0,0,width,height);

          // Show resized image in preview element
          var dataurl = canvas.toDataURL(file.type);
          document.getElementById("preview").src = dataurl;
          setImage({ image: dataurl });
        };
        img.src = e.target.result;
      };
    } else {
      return false;
    }
    console.log("image", image.image);
    var t = file.type.split("/").pop().toLowerCase();
    if (t != "jpeg" && t != "jpg" && t != "png") {
      setError("Please select a valid image file(ext [.jpeg || .jpg || .png])");
      e.target.value = "";
      return false;
    }
    if (file.size > 2048000) {
      setError("Max Upload size is 2MB only");
      e.target.value = "";
      return false;
    }

    setError("");

    // reader.addEventListener(
    //   "load",
    //   () => {
    //     setImage({ image: reader.result });
    //   },
    //   false
    // );

    if (file.type.includes("image/")) reader.readAsDataURL(file);
  };

  const handleSubmit = preventDefault(() => {
    dispatch({
      type: "ADD_SUMMARY",
      payload: {
        summary,
        image,
        file: file.name,
      },
    });
    router.push("/choose/resume");
  });

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col">
          <h2 className="text-center">SUMMARY</h2>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="summary">Summary</Label>
              <Input
                type="textarea"
                name="summary"
                value={summary}
                onChange={handleParam(setSummary)}
                id="summary"
              />
              <span className="tips-summary">max. 150 words</span>
            </FormGroup>
            <div className="form-control">
              <Label for="file">Image</Label>
              <br />
              <Input
                type="file"
                value={file.name}
                accept="image/*"
                onChange={handleFileChange}
                name="file"
                id="file"
                required
              />
              <img src="" width="150" height="180" id="preview" alt="" />
              <br />
              <span className="text alert-danger" style={{ fontSize: 11 }}>
                {error}
              </span>
            </div>
            <br />
            <Button type="submit" className="btn-pastel">
              Save &amp; Next
            </Button>
          </Form>
        </div>
        <div>
          <p className="">Tip</p>
          <li>
            <span className="tips-summary">
              Sesuaikan summarymu untuk setiap pekerjaan yang dilamar
            </span>
          </li>
          <li>
            <span className="tips-summary">Fokus pada hasil yang spesifik</span>
          </li>
          <li>
            <span className="tips-summary">
              Tulis tipe organisasi dan industri yang kamu geluti sebelumnya
            </span>
          </li>
          <li>
            <span className="tips-summary">Hindari istilah-istilah umum</span>
          </li>
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps, null)(FormSummary);
