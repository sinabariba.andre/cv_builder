import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Jumbotron, Button, NavLink } from "reactstrap";
import { getFaculties, getProvince, getTitles, getUniv } from "../store/actions/api.actions";
import axios from 'axios'

const HeroComp = (props) => {
  const dispatch =useDispatch()
  useEffect(async()=>{
    await dispatch(getUniv)
    await dispatch(getProvince)
    await dispatch(getTitles)
    await dispatch(getFaculties)
    
  })

  return (
    <div className="hero-banner">
      <div className="hero-text">
        <Jumbotron className="text-center">
          <h1 className="">HELLO BUDDY!</h1>
          <p className="lead">Don't Waste Your Time, Create Your Resume Now !</p>
          <div className="lead text-center">
            <NavLink className="btn-started" href="/resume">
              <Button color="primary" >GET STARTED</Button>
            </NavLink>
          </div>
        </Jumbotron>
      </div>
    </div>
  );
};

export default HeroComp;
