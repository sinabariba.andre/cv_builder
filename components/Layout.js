import React, { Component } from "react";
import TopBar from "./TopBar";
import Footer from "./Footer";

class Layout extends Component {
  render() {
    const { children } = this.props;
    return (
      <div className="container-fluid p-0">
        <TopBar />
        {children}
        <Footer />
      </div>
    );
  }
}

export default Layout;
