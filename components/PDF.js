import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  BlobProvider,
} from "@react-pdf/renderer";

const mapStateToProps = (state) => ({
  person: state.person,
  education: state.education,
  experience: state.experience,
  skill: state.skill,
  summary: state.summary,
});

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
    backgroundColor: "#fff",
    width: "100%",
    orientation: "portrait",
  },
  title: {
    fontSize: "16pt",
    fontWeight: "bold",
    color: "salmon",
    lineHeight: "10px",
  },
  col_kiri: {
    backgroundColor: "salmon",
    width: "30%",
    position: "absolute",
    width: "100%",
    height: "100%",
    padding: 0,
  },
  col_kanan: {
    backgroundColor: "aqua",
    display: "inline",
    position: "absolute",
    width: "100%",
    height: "100%",
    padding: 0,
    right: "0px",
    width: "70%",
  },
  section: {
    margin: 2,
    padding: 5,
    // textAlign: "center",
    fontSize: "12pt",
  },
});

const pdfText = ({ person, education, experience, skill, summary }) => {
  console.log('edu',JSON.stringify(education.education.school))
  return (
    <Document>
      <Page size="A4" className="container">
        <View style={styles.col_kiri}>
          <Text style={styles.title}>Parodi Hujan</Text>
          <View style={styles.section} className="text-center">
            <Text>
              Nama : {person.personalinfo.fname} {person.personalinfo.lname}
            </Text>
            <Text className="text-primary">Hoby :hoby</Text>
            <Text>Alamat : alamt</Text>
            <Text>Tak ada yang lebih basah</Text>
            <Text>dari hujan setelah Oktober. Banjir</Text>
          </View>
        </View>
        <View style={styles.col_kanan}>
          <Text style={styles.title}>Parodi Hujan</Text>
          <View style={styles.section} className="text-center">
            <Text>
              Nama : {person.personalinfo.fname} {person.personalinfo.lname}
            </Text>
            <Text className="text-primary">Hoby :hoby</Text>
            <Text>Alamat : alamt</Text>
            <Text>Tak ada yang lebih basah</Text>
            <Text>dari hujan setelah Oktober. Banjir</Text>
          </View>
        </View>
      </Page>
    </Document>
  );
};
class PDF extends Component {
  render() {
    const { person, education, experience, skill, summary } = this.props;
    return (
      <div style={{ height: "720px" }}>
        <BlobProvider
          document={pdfText({ person, education, experience, skill, summary })}
        >
          {({ url }) => (
            <iframe src={url} style={{ width: "100%", height: "100%" }} />
          )}
        </BlobProvider>
      </div>
    );
  }
}
export default connect(mapStateToProps, null)(PDF);
