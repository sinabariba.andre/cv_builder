import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
} from "reactstrap";

const TopBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  const dispatch = useDispatch();
  const resetHistory = ()=>{
    dispatch({type:'RESET_EDUCATION'})
    dispatch({type:'RESET_EXPERIENCE'})
    dispatch({type:'RESET_PERSON'})
    dispatch({type:'RESET_SKILL'})
    dispatch({type:'RESET_SUMMARY'})
  }
  

  return (
    <>
      <Navbar light expand="md" className="topbar">
        <NavbarBrand className="brand" href="/">
          Brand
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="topbar-link mr-auto" navbar>
            <NavItem className="">
              <NavLink href="/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={()=>resetHistory()} href="/resume">New Resume</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/tips">Tips</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/about">About</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </>
  );
};

export default TopBar;
