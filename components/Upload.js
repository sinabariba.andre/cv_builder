import React, { Component } from "react";
import { Label, Input, FormText, FormGroup } from "reactstrap";

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = { pictures: [] };
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(picture) {
    this.setState({
      pictures: this.state.pictures.concat(picture),
    });
  }
  render() {
    return (
      <div className="row justify-content-center ">
        <div className="col col-md-6">
          <FormGroup>
            <Label for="exampleFile">File</Label>
            <Input type="file" name="file" id="exampleFile" />
            <FormText color="muted">
              This is some placeholder block-level help text for the above
              input. It's a bit lighter and easily wraps to a new line.
            </FormText>
          </FormGroup>
        </div>
      </div>
    );
  }
}
export default Upload;
