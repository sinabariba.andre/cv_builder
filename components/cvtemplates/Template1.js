import React, { Component } from "react";
import { connect } from "react-redux";
// styles
import styles from "../../styles/Home.module.css";

const mapStateToProps = (state) => ({
  person: state.person,
  education: state.education,
  experience: state.experience,
  skill: state.skill,
  summary: state.summary,
});

const template1Preview = ({
  person,
  education,
  experience,
  skill,
  summary,
}) => {
  return (
    <div className={styles.template1container}>
      <div className={styles.template1containerkiri}>
        <img
          className={styles.template1photo}
          src="https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cmFuZG9tJTIwcGVyc29ufGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80"
          alt="FOTONE RAIMU"
        />
        <div className={styles.template1socialmedia}>
          <img src="" alt="" />
          <p>{person.personalinfo.fname} {person.personalinfo.lname}</p>
          <img src="" alt="" />
          <p>TWITTER</p>
          <img src="" alt="" />
          <p>GITHUB</p>
          <img src="" alt="" />
          <p>LINKEDIN</p>
          <img src="" alt="" />
          <p>INSTAGRAM</p>
          <img src="" alt="" />
          <p>CODEPEN</p>
        </div>
        <div className={styles.template1skill}>
          <h2 className={styles.template1SectionTitle}>Skill</h2>
          <p>
            {skill.skill.skill1} {skill.skill.level1}
          </p>
          <p>
            {skill.skill.skill2} {skill.skill.level2}
          </p>
          <p>
            {skill.skill.skill3} {skill.skill.level3}
          </p>
        </div>
      </div>
      <div className={styles.template1containerkanan}>
        <div className={styles.template1Summary}>
          <h2 className={styles.template1SectionTitle}>Summary</h2>
          <p>{summary.summary.summary}</p>
        </div>
        <div className={styles.template1Education}>
          <h2 className={styles.template1SectionTitle}>Academic Background</h2>
          <p>{education.education.school}</p>
          <p>{education.education.city}</p>
          <p>{education.education.provinsi}</p>
          <p>{education.education.fakultas}</p>
          <p>{education.education.gelar}</p>
          <p>{education.education.startdate}</p>
          <p>{education.education.enddate}</p>
        </div>
        <div className={styles.template1Experiences}>
          <h2 className={styles.template1SectionTitle}>Working Experiences</h2>
          <p>{experience.experience.employer}</p>
          <p>{experience.experience.jobtitle}</p>
          <p>{experience.experience.city}</p>
          <p>{experience.experience.phone}</p>
          <p>{experience.experience.career}</p>
          <p>{experience.experience.jobdesc}</p>
          <p>{experience.experience.startdate}</p>
          <p>{experience.experience.enddate}</p>
        </div>
      </div>
    </div>
  );
};

class Template1 extends Component {
  render() {
    const { person, education, experience, skill, summary } = this.props;
    return (
      <div>
        {template1Preview({ person, education, experience, skill, summary })}
      </div>
    );
  }
}
export default connect(mapStateToProps, null)(Template1);
