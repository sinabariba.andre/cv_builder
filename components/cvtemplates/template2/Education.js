import React from "react";
import { Text, View, StyleSheet, List, Item } from "@react-pdf/renderer";

import Title from "./Title";

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  title: {
    fontFamily: "Lato Bold",
    fontSize: 10,
  },
  school: {
    fontFamily: "Lato Italic",
    fontSize: 10,
  },
  date: {
    fontFamily: "Lato Bold",
    fontSize: 9,
  },
  degree: {
    fontFamily: "Lato",
    fontSize: 10,
  },
  candidate: {
    fontFamily: "Lato Italic",
    fontSize: 10,
  },
});

const EduEntry = ({ school, city, dates, title }) => {
  const date = `${dates[0]} ${dates[1]} - ${dates[2]} ${dates[3]}`;
  return (
    <View style={styles.container}>
      <Text style={styles.date}>{date}</Text>
      <Text style={styles.date}>{title}</Text>
      <Text style={styles.school}>{school}, {city}</Text>

      <Text style={styles.candidate}></Text>
    </View>
  );
};

const Education = (props) => {
  const { education } = props.data;

  return (
    <View style={styles.container}>
      <Title>Education</Title>
      {Object.values(education.edu).map(
        ({ school, city, title, startmonth, startyear, endmonth, endyear }) => {
          
          return (
          <EduEntry key={Math.random()-Date.now()}
            school={school}
            city={city}
            dates={[startmonth, startyear, endmonth, endyear]}
            title={title}
          />
        )}
      )}
    </View>
  );
};
export default Education;
