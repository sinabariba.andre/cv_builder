import React from "react";
import { Text, View, StyleSheet, List, Item } from "@react-pdf/renderer";

import Title from "./Title";

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  title: {
    fontFamily: "Lato Bold",
    fontSize: 10,
  },
  profile: {
    fontFamily: "Lato",
    fontSize: 10,
    textTransform: "capitalize",
  },
});

const InfoEntry = ({ address, city, phonenumber, country, zipcode }) => {
  return (
    <View style={styles.container}>
      <Text key={zipcode} style={styles.profile}>
        {address}, {zipcode}
      </Text>
      <Text key={city} style={styles.profile}>
        {city}, {country}
      </Text>
      <Text key={phonenumber} style={styles.profile}>{phonenumber}</Text>
    </View>
  );
};

function Info(props) {
  const { person } = props.data;

  return (
    <View style={styles.container}>
      <Title>Personal Info</Title>
      {Object.values(person).map(
        ({ address, city, country, zipcode, phonenumber }) => {
            let i=0;
          return (
            <InfoEntry key={i}
              address={address}
              city={city}
              country={country}
              zipcode={zipcode}
              phonenumber={phonenumber}
            />
          );
        }
      )}
    </View>
  );
}

export default Info;
