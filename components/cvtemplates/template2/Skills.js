/* eslint-disable react/no-array-index-key */

import React from "react";
import { Text, View, StyleSheet } from "@react-pdf/renderer";

import Title from "./Title";
import List, { Item } from "./List";

const styles = StyleSheet.create({
  title: {
    fontFamily: "Lato Bold",
    fontSize: 11,
    marginBottom: 10,
  },
  skills: {
    fontFamily: "Lato",
    fontSize: 10,
    marginBottom: 10,
  },
  skill: {
    fontFamily: "Lato",
    fontSize: 10,
    marginBottom: 10,
  },
});

const SkillEntry = ({ name, skills }) => (
  <View>
    <Text style={styles.title}>{name}</Text>
    <List>
      {skills.map((skill, i) => (
        <Item key={i}>{skill}</Item>
      ))}
    </List>
  </View>
);

const Skills = (props) => {
  const {skill} = props.data
  return (
    <View>
      <Title>Skills</Title>
      <View>
      <Text style={styles.skill}>  {skill.skill.skill1} - {skill.skill.level1} </Text>
      <Text style={styles.skill}>  {skill.skill.skill2} - {skill.skill.level2} </Text>
      <Text style={styles.skill}>  {skill.skill.skill3} - {skill.skill.level3} </Text>
      </View>
      {/* <SkillEntry
        name="Combat Abilities"
        skills={[
          "Completed Jedi Master training and built a lightsaber from scratch in order to do battle against the Empire",
          "Defeated the Rancor and rescued Princess Leia from Jabba the Hutt",
          "Competent fighter pilot as well as an excelent shot with nearly any weapon",
        ]}
      /> */}
    </View>
  );
};
export default Skills;
