import React from "react";
import { Text, View, StyleSheet, List, Item } from "@react-pdf/renderer";
import Title from './Title'

const styles = StyleSheet.create({
    container: {
        width:'100%',
      flexDirection:'column',
      paddingTop: 30,
      paddingLeft: 15,
      '@media max-width: 400': {
        paddingTop: 10,
        paddingLeft: 0,
      },
    },
    entryContainer: {
        width:'100%',
    },
    summary: {
      fontSize: 12,
      fontFamily: 'Lato Italic',
      textAlign:'justify',
    },
    
  });

function Summary(props) {
  const {summary} = props.data
  return (
    <View style={styles.container}>
    <Title>Summary</Title>
    <View style={styles.entryContainer}>
        {Object.values(summary).map(val=>(
          <Text key={val} style={styles.summary}>{val.summary}</Text>

        ))}
    </View>
   
  </View>
  );
}

export default Summary;
