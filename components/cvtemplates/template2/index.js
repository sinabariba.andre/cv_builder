import React from "react";
import { connect } from "react-redux";
import {
  Text,
  BlobProvider,
  Font,
  Page,
  View,
  Image,
  Document,
  StyleSheet,
} from "@react-pdf/renderer";

import Header from "./Header";
import Skills from "./Skills";
import Education from "./Education";
import Experience from "./Experience";
import Summary from "./Summary";
import Info from "./Info";

const mapStateToProps = (state) => ({
  person: state.person,
  education: state.education,
  experience: state.experience,
  skill: state.skill,
  summary: state.summary,
});

const styles = StyleSheet.create({
  page: {
    padding: 30,
  },
  container: {
    flex: 1,
    flexDirection: "row",
    "@media max-width: 400": {
      flexDirection: "column",
    },
  },
  borderImg: {
    width: 150,
    height: 185,
  },
  image: {
    marginBottom: 10,
  },
  leftColumn: {
    flexDirection: "column",
    width: 170,
    paddingTop: 30,
    paddingRight: 15,
    "@media max-width: 400": {
      width: "100%",
      paddingRight: 0,
    },
    "@media orientation: landscape": {
      width: 200,
    },
  },
  rightColumn: {
    flexDirection: "column",
    width: "70%",
    "@media max-width: 400": {
      width: "100%",
      paddingRight: 0,
    },
    "@media orientation: landscape": {
      width: 200,
    },
  },
  footer: {
    fontSize: 12,
    fontFamily: "Lato Bold",
    textAlign: "center",
    marginTop: 15,
    paddingTop: 5,
    borderWidth: 3,
    borderColor: "gray",
    borderStyle: "dashed",
    "@media orientation: landscape": {
      marginTop: 10,
    },
  },
});

Font.register({
  family: "Open Sans",
  src: `https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFVZ0e.ttf`,
});

Font.register({
  family: "Lato",
  src: `https://fonts.gstatic.com/s/lato/v16/S6uyw4BMUTPHjx4wWw.ttf`,
});

Font.register({
  family: "Lato Italic",
  src: `https://fonts.gstatic.com/s/lato/v16/S6u8w4BMUTPHjxsAXC-v.ttf`,
});

Font.register({
  family: "Lato Bold",
  src: `https://fonts.gstatic.com/s/lato/v16/S6u9w4BMUTPHh6UVSwiPHA.ttf`,
});

const Resume = (props) => {
  const { summary } = props.data;
  const img = Object.values(summary.summary.image);
  const filename = summary.summary.file;
  return (
    <Page {...props} style={styles.page}>
      <Header {...props} />
      <View style={styles.container}>
        <View style={styles.leftColumn}>
          <View style={styles.borderImg}>
            <Image style={styles.image} src={img + filename} />
          </View>
          <Info {...props} />
          <Education {...props} />
          <Skills {...props} />
        </View>
        <View style={styles.rightColumn}>
          <Summary {...props} />
          <Experience {...props} />
        </View>
      </View>
      {/* <Text style={styles.footer}>This IS the candidate you are looking for</Text> */}
    </Page>
  );
};

const PDFtext = (props) => (
  <Document
    author="Andre Sinabariba"
    keywords="awesome, resume, start wars"
    subject="The resume of Luke Skywalker"
    title="Resume"
  >
    <Resume size="A4" data={props} />
    <Resume orientation="landscape" size="A4" data={props} />
    {/* <Resume size={[380, 1250]}  data={props}/> */}
  </Document>
);

class PDFResume extends React.Component {
  render() {
    const { person, education, experience, skill, summary } = this.props;
    return (
      <div style={{ height: "720px" }}>
        <BlobProvider
          document={PDFtext({ person, education, experience, skill, summary })}
        >
          {({ url }) => (
            <iframe src={url} style={{ width: "100%", height: "100%" }} />
          )}
        </BlobProvider>
      </div>
    );
  }
}

export default connect(mapStateToProps, null)(PDFResume);
