import axios from "axios";

export const months = Array.from({ length: 12 }, (item, i) => {
  return new Date(0, i).toLocaleString("en-US", { month: "short" });
});

export const generateArrayOfYears = () => {
  var max = new Date().getFullYear();
  var min = max - 100;
  var years = [];
  for (var i = max; i >= min; i--) {
    years.push(i);
  }
  return years;
};

export const getProvinsi = async () => {
  const res = await axios
    .get("https://dev.farizdotid.com/api/daerahindonesia/provinsi")
    .then((response) => {
      // console.log('globalobject',response.data.provinsi)
      return response.data.provinsi;
    })
    .catch((err) => {
      console.log(err);
    });
  return res;
};


