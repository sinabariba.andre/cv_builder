const fs = require('fs')

const faculties = require('../../masterdata/fakultas.json')
export default function handlerFaculties(req,res){
  res.status(200).json(faculties)
}