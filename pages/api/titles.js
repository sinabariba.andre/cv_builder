const fs = require('fs')

const titles = require('../../masterdata/title.json')
export default function handlerTitles(req,res){
  res.status(200).json(titles)
}