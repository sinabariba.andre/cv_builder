const fs = require('fs')

const universitas = require('../../masterdata/universitas.json')
export default function handlerUniversitas(req,res){
  res.status(200).json(universitas)
}