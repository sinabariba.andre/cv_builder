import React from "react";
import Head from "next/head";
import { connect } from "react-redux";
const mapStateToProps = (state) => ({
  person: state.person,
  education: state.education,
  experience: state.experience,
  skill: state.skill,
  summary: state.summary,
});

function simple_resume({ person, education, experience, skill, summary }) {

    const startdate=new Date(education.education.startdate)
  return (
    <div>
      <Head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Resume</title>
        <link type="text/css" rel="stylesheet" href="/assets/css/blue.css" />
        <link
          type="text/css"
          rel="stylesheet"
          href="/assets/css/print.css"
          media="print"
        />

        <link href="/assets/css/ie7.css" rel="stylesheet" type="text/css" />

        <link href="/assets/css/ie6.css" rel="stylesheet" type="text/css" />

        {/* <script type="text/javascript" src="/assets/js/cufon.yui.js"></script> */}
        <script type="text/javascript" src="/assets/js/myriad.js"></script>
      </Head>

      <div id="wrapper">
        <div className="wrapper-top"></div>
        <div className="wrapper-mid">
          <div id="paper">
            <div className="paper-top"></div>
            <div id="paper-mid">
              <div className="entry">
                <img
                  className="portrait"
                  src="/assets/images/image.jpg"
                  alt="John Smith"
                />

                <div className="self col-6">
                  <h1 className="name">
                    {person.personalinfo.fname} {person.personalinfo.lname}
                    <br />
                    <span>Interactive Designer</span>
                  </h1>
                  <ul>
                    <li className="ad">
                      {person.personalinfo.address},{" "}
                      {person.personalinfo.zipcode}, {person.personalinfo.city}
                    </li>
                    <li className="mail">{person.personalinfo.email}</li>
                    <li className="tel">{person.personalinfo.phonenumber}</li>
                    <li className="web">www.businessweb.com</li>
                  </ul>
                </div>

                <div className="social">
                  <ul>
                    <li>
                      <a className="north" href="#" title="Download .pdf">
                        <img
                          src="/assets/images/icn-save.jpg"
                          alt="Download the pdf version"
                        />
                      </a>
                    </li>
                    <li>
                      <a
                        className="north"
                        href="javascript:window.print()"
                        title="Print"
                      >
                        <img src="/assets/images/icn-print.jpg" alt="" />
                      </a>
                    </li>
                    <li>
                      <a
                        className="north"
                        id="contact"
                        href="contact/index.html"
                        title="Contact Me"
                      >
                        <img src="/assets/images/icn-contact.jpg" alt="" />
                      </a>
                    </li>
                    <li>
                      <a
                        className="north"
                        href="#"
                        title="Follow me on Twitter"
                      >
                        <img src="/assets/images/icn-twitter.jpg" alt="" />
                      </a>
                    </li>
                    <li>
                      <a className="north" href="#" title="My Facebook Profile">
                        <img src="/assets/images/icn-facebook.jpg" alt="" />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="entry">
                <h2>SUMMARY</h2>
                <p>
                {summary.summary.summary}
                </p>
              </div>

              <div className="entry">
                <h2>EDUCATION</h2>
                <div className="content">
                  <h4>{education.education.startmonth} {education.education.startyear}-{education.education.endmonth} {education.education.endyear}</h4>
                  <p>
                    {education.education.school}, {education.education.city} <br />
                    <em>Master in Communication Design</em>
                  </p>
                </div>
                <div className="content">
                  <h3>Sep 2001 - Jun 2005</h3>
                  <p>
                    University of Art &amp; Design, New York <br />
                    <em>Bachelor of Science in Graphic Design</em>
                  </p>
                </div>
              </div>

              <div className="entry">
                <h2>EXPERIENCE</h2>
                <div className="content">
                  <h3>{experience.experience.startmonth} {experience.experience.startyear} - {experience.experience.endmonth} {experience.experience.endyear}</h3>
                  <p>
                    {experience.experience.employer}, {experience.experience.city} <br />
                    <em>{experience.experience.career}</em>
                  </p>
                  <ul className="info">
                    <li>Vestibulum eu ante massa, sed rhoncus velit.</li>
                    <li>
                      Pellentesque at lectus in <a href="#">libero dapibus</a>{" "}
                      cursus. Sed arcu ipsum, varius at ultricies acuro,
                      tincidunt iaculis diam.
                    </li>
                  </ul>
                </div>
                <div className="content">
                  <h3>Jun 2007 - May 2009</h3>
                  <p>
                    Junior Web Designer <br />
                    <em>Bachelor of Science in Graphic Design</em>
                  </p>
                  <ul className="info">
                    <li>
                      Sed fermentum sollicitudin interdum. Etiam imperdiet
                      sapien in dolor rhoncus a semper tortor posuere.{" "}
                    </li>
                    <li>
                      Pellentesque at lectus in libero dapibus cursus. Sed arcu
                      ipsum, varius at ultricies acuro, tincidunt iaculis diam.
                    </li>
                  </ul>
                </div>
              </div>

              <div className="entry">
                <h2>SKILLS</h2>
                <div className="content">
                  <h3>Software Knowledge</h3>
                  <ul className="skills">
                    <li>Photoshop</li>
                    <li>Illustrator</li>
                    <li>InDesign</li>
                    <li>Flash</li>
                    <li>Fireworks</li>
                    <li>Dreamweaver</li>
                    <li>After Effects</li>
                    <li>Cinema 4D</li>
                    <li>Maya</li>
                  </ul>
                </div>
                <div className="content">
                  <h3>Languages</h3>
                  <ul className="skills">
                    <li>CSS/XHTML</li>
                    <li>PHP</li>
                    <li>JavaScript</li>
                    <li>Ruby on Rails</li>
                    <li>ActionScript</li>
                    <li>C++</li>
                  </ul>
                </div>
              </div>

              <div className="entry">
                <h2>WORKS</h2>
                <ul className="works">
                  <li>
                    <a
                      href="/assets/images/1.jpg"
                      rel="gallery"
                      title="Lorem ipsum dolor sit amet."
                    >
                      <img src="/assets/images/image.jpg" alt="" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="/assets/images/2.jpg"
                      rel="gallery"
                      title="Lorem ipsum dolor sit amet."
                    >
                      <img src="/assets/images/image.jpg" alt="" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="/assets/images/3.jpg"
                      rel="gallery"
                      title="Lorem ipsum dolor sit amet."
                    >
                      <img src="/assets/images/image.jpg" alt="" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="/assets/images/1.jpg"
                      rel="gallery"
                      title="Lorem ipsum dolor sit amet."
                    >
                      <img src="/assets/images/image.jpg" alt="" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="/assets/images/2.jpg"
                      rel="gallery"
                      title="Lorem ipsum dolor sit amet."
                    >
                      <img src="/assets/images/image.jpg" alt="" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="/assets/images/3.jpg"
                      rel="gallery"
                      title="Lorem ipsum dolor sit amet."
                    >
                      <img src="/assets/images/image.jpg" alt="" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="/assets/images/1.jpg"
                      rel="gallery"
                      title="Lorem ipsum dolor sit amet."
                    >
                      <img src="/assets/images/image.jpg" alt="" />
                    </a>
                  </li>
                  <li>
                    <a
                      href="/assets/images/1.jpg"
                      rel="gallery"
                      title="Lorem ipsum dolor sit amet."
                    >
                      <img src="/assets/images/image.jpg" alt="" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="clear"></div>
            <div className="paper-bottom"></div>
          </div>
        </div>
        <div className="wrapper-bottom"></div>
      </div>
      <div id="message">
        <a href="#top" id="top-link">
          Go to Top
        </a>
      </div>
    </div>
  );
}

export default connect(mapStateToProps, null)(simple_resume);
