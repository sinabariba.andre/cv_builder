import axios from "axios";
export const GET_UNIV = "GET_UNIV";
export const GET_PROVINCE = "GET_PROVINCE";
export const GET_CITY = "GET_CITY";
export const GET_FACULTIES = "GET_FACULTIES";
export const GET_TITLES = "GET_TITLES";

export const getUniv = async (dispatch) => {
  const response = axios
    .get("https://intense-crag-30726.herokuapp.com/api/universitas")
    .then((res) => {
      dispatch({
        type: "GET_UNIV",
        payload: res.data,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const getProvince = async (dispatch) => {
  const response = axios
    .get("https://dev.farizdotid.com/api/daerahindonesia/provinsi")
    .then((res) => {
      dispatch({
        type: "GET_PROVINCE",
        payload: res.data,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const getTitles = async (dispatch) => {
  const response = await axios
    .get("https://intense-crag-30726.herokuapp.com/api/titles")
    .then((res) => {
      dispatch({
        type: "GET_TITLES",
        payload: res.data,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};

export const getFaculties = async (dispatch) => {
  const response = await axios
    .get("https://intense-crag-30726.herokuapp.com/api/faculties")
    .then((res) => {
      dispatch({
        type: "GET_FACULTIES",
        payload: res.data,
      });
    })
    .catch(function (error) {
      console.log(error);
    });
};
