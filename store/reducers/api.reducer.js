import {GET_PROVINCE, GET_UNIV,GET_CITY,GET_FACULTIES,GET_TITLES} from '../actions/api.actions'
const initialState = {
    univ:[],
    province:[],
    city:[],
    titles:[],
    faculties:[]

}

export const API = (state = initialState, action) => {
    switch (action.type) {

    case GET_UNIV:
        return { ...state, univ:action.payload }
    case GET_PROVINCE:
        return { ...state, province:action.payload }
    case GET_CITY:
        return { ...state, city:action.payload }
    case GET_TITLES:
        return { ...state, titles:action.payload }
    case GET_FACULTIES:
        return { ...state, faculties:action.payload }

    default:
        return state
    }
}

export default API