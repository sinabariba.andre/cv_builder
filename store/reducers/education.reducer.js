import { ADD_EDUCATION, RESET_EDUCATION } from "../actions/education.action";

const initialState = {
  edu:[]
};
const update_edu = (state, mutations) => Object.assign({}, state, mutations);

export const education_reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_EDUCATION:
      return {
       ...(state = update_edu(state, {
          edu: state.edu.concat(action.payload),
        // ...(state = update_edu(state, {
        //   education: state.concat(action.payload),
        })),
      };
    case RESET_EDUCATION:
      return initialState;
      break;
    default:
      return state;
  }
};

export default education_reducer;
