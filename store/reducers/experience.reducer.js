import { ADD_EXPERIENCE, RESET_EXPERIENCE } from "../actions/experience.action";

const initialState = {
  exp:[]
};
const update_exp = (state, mutations) => Object.assign({}, state, mutations);

export const experience_reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_EXPERIENCE:
      return {
        ...(state = update_exp(state, {
          exp: state.exp.concat(action.payload),
          
        })),
      };

    case RESET_EXPERIENCE:
      return initialState;

    default:
      return state;
  }
};

export default experience_reducer;
