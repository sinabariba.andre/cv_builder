import { ADD_PERSON, RESET_PERSON } from "../actions/personalinfo.action";

const initialState = {
  personalinfo: [],
};

export const person_reducer= (state = initialState, action) => {
  switch (action.type) {
    case ADD_PERSON:
      return { personalinfo: action.payload };
    case RESET_PERSON:
      return initialState;

    default:
      return state;
  }
};

export default person_reducer