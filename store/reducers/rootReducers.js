import { combineReducers } from "redux";
import {persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import person from "./personalinfo.reducer";
import education from "./education.reducer";
import experience from "./experience.reducer";
import skill from "./skill.reducer";
import summary from "./summary.reducer";
import API from './api.reducer'

const persistConfig ={
  key:'root',
  storage,
  whitelist:['person','education','experience','skill','summary','API']
}


const rootReducer = combineReducers({

  person: person,
  education: education,
  experience:experience,
  skill:skill,
  summary:summary,
  API:API,
});

export default persistReducer(persistConfig, rootReducer)
