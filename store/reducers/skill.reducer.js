import { ADD_SKILL, RESET_SKILL } from "../actions/skill.action";

const initialState = {};

export const skill_reducer= (state = initialState, action) => {
  switch (action.type) {
    case ADD_SKILL:
      return { skill: action.payload };

    case RESET_SKILL:
      return initialState

    default:
      return state;
  }
};

export default skill_reducer