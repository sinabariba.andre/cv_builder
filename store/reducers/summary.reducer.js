import { ADD_SUMMARY, RESET_SUMMARY } from "../actions/summary.action";

const initialState = {};

export const summary_reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_SUMMARY:
      return { summary: action.payload };
    case RESET_SUMMARY:
      return initialState;

    default:
      return state;
  }
};

export default summary_reducer;
